<?php
// Translates acf custom field ids with polylang for new posts 
// http://www.advancedcustomfields.com/resources/filters/acfload_value/
// Usage: add_filter('acf/load_value/name=FIELD_NAME', 'acf_polylang_translate_newpost_ids', 10, 3);
function acf_polylang_translate_newpost_ids( $value, $post_id, $field ) {
	if ( isset( $_GET['from_post'] ) && is_numeric( $_GET['from_post'] ) && (is_numeric( $value ) || is_array( $value )) && isset( $_GET['post_type'] ) && isset( $_GET['new_lang'] ) ) {
		$new_value = polylang_custom_translate_ids( $value, $_GET['new_lang'] );
		if ( ! empty( $new_value ) ) {
			$value = $new_value;
		}
	}
    return $value;
}

// Copy acf custom field value from original post for new posts 
// http://www.advancedcustomfields.com/resources/filters/acfload_value/
// Usage: add_filter( 'acf/load_field/name=FIELD_NAME', 'acf_polylang_copy_newpost_fields' );
function acf_polylang_copy_newpost_fields ( $field ) {
	if ( isset( $_GET['from_post'] ) && is_numeric( $_GET['from_post'] ) && isset( $_GET['post_type'] ) && isset( $_GET['new_lang'] ) ) {
		$from_post = get_post( (int)$_GET['from_post'], OBJECT, 'edit' );
		if ( isset( $field['post_field_type'] ) ) {
			if ( $field['post_field_type'] == 'post_title' ) {
				$field['default_value'] = $from_post->post_title;
			} else if ( $field['post_field_type'] == 'excerpt' ) {
				$field['default_value'] = $from_post->post_excerpt;
			} else if ( $field['post_field_type'] == 'content' ) {
				$field['default_value'] = $from_post->post_content;
			}
		}
	}
    return $field;
}

function polylang_custom_translate_ids( $ids, $new_lang_slug, $type = 'post' ) {
	$new_lang_id = '';
	if ( is_array( $ids ) ) {
		$new_lang_ids = array();
		foreach( $ids as $id ) {
			if ( $type == 'post' ) {
				$new_lang_id = pll_get_post( $id, $new_lang_slug );
			} else if ( $type == 'term' ) {
				$new_lang_id = pll_get_term( $id, $new_lang_slug );
			}
			if ( ! empty( $new_lang_id ) ) {
				$new_lang_ids[] = $new_lang_id;
			}
		}
		
		if ( ! empty( $new_lang_ids ) ) {
			return $new_lang_ids;
		}
	} else if ( is_numeric( $ids ) ) {
		if ( $type == 'post' ) {
			$new_lang_id = pll_get_post( $id, $new_lang_slug );
		} else if ( $type == 'term' ) {
			$new_lang_id = pll_get_term( $id, $new_lang_slug );
		}
		
		if ( ! empty( $new_lang_id ) ) {
			return $new_lang_id;
		}
	}
	return false;
}

class polylang_custom_post_sync {
	public $polylang;
	public $post_id;
	public $current_language_slug;
	public $post_translations;
	private $syncable = false;
	
	function __construct( $post_id ) {
		global $polylang;
		$post_status = get_post_status( $post_id );
		if ( is_object( $polylang ) && $post_status != false && $post_status != 'auto-draft' ) {
			if ( $polylang->model->is_translated_post_type( get_post_type( $post_id ) ) ) {
				$this->post_id = $post_id;
				$this->polylang = $polylang;
				
				if ( isset( $_GET['new_lang'] ) ) {
					$this->current_language_slug = $_GET['new_lang'];
				} else {
					$this->current_language_slug = $polylang->model->get_post_language( $post_id )->slug;
				}
				
				if ( isset( $_GET['from_post'] ) ) {
					$this->post_translations = $polylang->model->get_translations( get_post_type( intval( $_GET['from_post'] ) ), intval( $_GET['from_post'] ) );
				} else {
					$this->post_translations = $polylang->model->get_translations( get_post_type( $post_id ), $post_id );
				}
				$this->syncable = true;
			} 
		} 
	}
	
	public function post_title() {
		if ( $this->syncable == false ){ return; }
		$the_title = get_the_title( $this->post_id );
		//if ( $this->polylang->model->is_translated_post_type( get_post_type( $this->post_id ) ) ) {
			foreach( $this->post_translations as $this_language_slug => $this_language_post_id ) {
				if ( $this_language_slug != $this->current_language_slug ) {
					$this_language_post = get_post( intval( $this_language_post_id ) );
					if ( $this_language_post->post_title != $the_title && $this->post_id != $this_language_post->post_id ) {
						$this_language_post->post_title = $the_title;
						//wp_update_post( $this_language_post );
						global $wpdb;
						//$wpdb->update( $wpdb->posts, array("post_title" => $the_title), array("post_id" => $this_language_post_id), array("%s"), array("%d") );
						$where = array( 'ID' => intval( $this_language_post_id ) );
						$wpdb->update( $wpdb->posts, array( 'post_title' => $the_title ), $where );						
					}
				}
			}
		//}
	}
	
	// Sync the post terms for non translatable taxonomy
	public function non_translatable_post_terms( $taxonomy ) {
		if ( $this->syncable == false ){ return; }
		if ( ! in_array( $taxonomy, $this->polylang->model->taxonomies ) ) {
			$terms_ids = wp_get_post_terms( $this->post_id, $taxonomy, array( "fields" => "ids" ) );
			$terms_ids = array_map('intval', $terms_ids);
			foreach( $this->post_translations as $this_language_slug => $this_language_post_id ) {
				if ( $this_language_slug != $this->current_language_slug ) {
					wp_set_object_terms( $this_language_post_id, $terms_ids, $taxonomy, false );
				}
			}
		}
	}
	
	// Sync and translate the post custom fields storing object ids
	function smartsync_custom_field_ids( $post_meta_key, $arg = array() ) {
		if ( $this->syncable == false ){ return; }
		$ids = get_post_meta( $this->post_id, $post_meta_key, true );
		if ( ! empty( $ids ) && ! is_array( $ids ) ) {
			$ids = array( $ids );
		}
		if ( ! empty( $arg['type'] ) && ! empty( $arg['slug'] ) && $arg['type'] == 'term' ) {
			
			$terms = get_terms( $arg['slug'], array( 'fields' => 'ids','hide_empty' => false, 'lang' => $this->current_language_slug) );
			$terms = array_map( 'intval', $terms );
			if ( is_array( $ids ) ) {
				$ids = array_map( 'intval', $ids );
				$ids_diff = array_diff( $terms, $ids );
				foreach( $this->post_translations as $this_language_slug => $this_language_post_id ) {
					if ( $this_language_slug != $this->current_language_slug ) {
						$this_lang_new_ids = polylang_custom_translate_ids( $ids, $this_language_slug, 'term' );
						
						$this_lang_ids_diff = polylang_custom_translate_ids( $ids_diff, $this_language_slug, 'term' );
						
						$this_lang_old_ids = get_post_meta( $this_language_post_id, $post_meta_key, true );
						$this_lang_old_ids = array_map( 'intval', $this_lang_old_ids );
						$this_lang_update_ids = array_merge( (array)$this_lang_old_ids, (array)$this_lang_new_ids );
						if ( is_array( $this_lang_update_ids ) && is_array( $this_lang_ids_diff ) ) {
							$this_lang_update_ids = array_diff( (array)$this_lang_update_ids, (array)$this_lang_ids_diff );
						}
						$this_lang_update_ids = array_unique( (array)$this_lang_update_ids );
						update_post_meta( $this_language_post_id, $post_meta_key, $this_lang_update_ids );
					}
				}
			}
			
		}
		return true;
	}	
	
	// Sync and translate the post custom fields storing object ids
	function translate_custom_field_ids( $post_meta_key ) {
		if ( $this->syncable == false ){ return; }
		$ids = get_post_meta( $this->post_id, $post_meta_key, true );
		if ( is_array( $ids ) ) {
			foreach( $this->post_translations as $this_language_slug => $this_language_post_id ) {
				if ( $this_language_slug != $this->current_language_slug ) {
					$this_lang_ids = polylang_custom_translate_ids( $ids, $this_language_slug );
					if ( $this_lang_ids == false ) {
						$this_lang_ids = polylang_custom_translate_ids( $ids, $this_language_slug, 'term' );
					}
					// Only sync if we found all the ids
					if ( count( $ids ) == count( $this_lang_ids ) ) {
						update_post_meta( $this_language_post_id, $post_meta_key, $this_lang_ids );
					} else {
						return false;
					}
				}
			}
		} else {
			//if ( ! empty( $ids ) ) {
				foreach( $this->post_translations as $this_language_slug => $this_language_post_id ) {
					if ( $this_language_slug != $this->current_language_slug ) {
						$this_lang_id = pll_get_post( $ids, $this_language_slug );
						if ( $this_lang_id == false ) {
							$this_lang_id = pll_get_term( $ids, $this_language_slug );
						}
						if ( is_numeric( $this_lang_id ) && $this_lang_id != $ids ) {
							update_post_meta( $this_language_post_id, $post_meta_key, $this_lang_id );
						} else if ( empty( $ids ) ) {
							update_post_meta( $this_language_post_id, $post_meta_key, '' );
						}	
					}
				}
			//}
		}
		return true;
	}
}
?>