<?php
/**
 * @package polylang-tweaks
 * @version 0.3.1
 */
/*
Plugin Name: Polylang Tweaks
Plugin URI: 
Description: Tweaks for the Polylang plugin
Author: Kim Helge Frimanslund
Version: 0.3
Author URI: 
*/

include( plugin_dir_path( __FILE__ ) . 'polylang-custom-sync.php');

add_action('admin_init', 'pll_remove_admin_language_filter', 2);
function pll_remove_admin_language_filter() {
	global $polylang;
	if ( ! is_object( $polylang ) ) { return; }
	remove_action('admin_bar_menu', array($polylang, 'admin_bar_menu'), 100);
	add_action( 'admin_bar_menu', 'polylang_custom_admin_bar_menu', 100 );
	add_filter( 'get_edit_term_link', 'polylang_custom_edit_term_link', 99, 3 );
	add_filter( 'get_edit_post_link', 'polylang_custom_edit_post_link', 99, 3 );
}

/*
 * adds the languages list in admin bar for the admin languages filter
 *
 * @since 0.1
 *
 * @param object $wp_admin_bar
 */
function polylang_custom_admin_bar_menu( $wp_admin_bar ) {
	global $polylang;
	if ( ! is_object( $polylang ) ) { return; }
	$url = ( is_ssl() ? 'https://' : 'http://' ) . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

	$all_item = (object) array(
		'slug' => 'all',
		'name' => __('Show all languages', 'polylang'),
		'flag' => '<span class="ab-icon"></span>'
	);

	$selected = empty($polylang->curlang) ? $all_item : $polylang->curlang;
	
	$wp_admin_bar->add_menu(array(
		'id'     => 'languages',
		'title'  => $selected->flag . '<span class="ab-label">'. esc_html($selected->name) . '</span>',
		'meta'  => array('title' => __('Filters content by language', 'polylang')),
	));	
	
	$screen = get_current_screen();
	if ( $screen->base == 'post' ) {
		if ( isset( $screen->post_type ) && pll_is_translated_post_type( $screen->post_type ) ) {
			if ( isset( $_GET['post'] ) ) {
				$post_id = get_the_ID();
				if ( $selected->slug != 'all' ) {
					$this_lang_post_id = pll_get_post( $post_id, $selected->slug );
					if ( ! $this_lang_post_id || isset( $_GET['lang_fail'] ) ) {
						add_action( 'admin_notices', 'polylang_custom_admin_notice' );
					}
				}
			} else if ( isset( $_GET['from_post'] ) ){
				$post_id = intval( $_GET['from_post'] );
				$url = remove_query_arg( array( 'from_post', 'new_lang' ) , $url );
			}
		}
	} else if ( $screen->base == 'edit-tags' ) {
		if ( isset( $screen->taxonomy ) && pll_is_translated_taxonomy( $screen->taxonomy ) ) {
			if ( isset( $_GET['tag_ID'] ) ) {
				$term_id = intval( $_GET['tag_ID'] );
				if ( $selected->slug != 'all' ) {
					$this_lang_term_id = $polylang->model->get_term( $term_id, $selected->slug );
					if ( ! $this_lang_term_id || isset( $_GET['lang_fail'] ) ) {
						add_action( 'admin_notices', 'polylang_custom_admin_notice' );//echo $polylang->model->get_term_language( $term_id )->slug;
					}
				}
			} else if ( isset( $_GET['from_tag'] ) ){
				$term_id = intval( $_GET['from_tag'] );
				$url = remove_query_arg( array( 'from_tag', 'new_lang' ) , $url );
			}
		}
	}

	foreach (array_merge(array($all_item), $polylang->model->get_languages_list()) as $lang) {
		if ( $selected->slug == $lang->slug )
			continue;
		
		if ( isset( $post_id ) && isset( $lang->locale ) && $lang->slug != 'all' ) {
			$this_lang_post_id = pll_get_post( $post_id, $lang->slug );
			if ( ! empty( $this_lang_post_id ) ) {
				$thisurl = get_edit_post_link( $this_lang_post_id );
			} else {
				$thisurl = get_edit_post_link( $post_id );
				$thisurl = add_query_arg( 'lang_fail', $lang->slug, $thisurl );
			}
		} else if ( isset( $term_id ) && isset( $lang->locale ) && $lang->slug != 'all' ) {
			$this_lang_term_id = $polylang->model->get_term( $term_id, $lang->slug );
			
			if ( ! empty( $this_lang_term_id ) ) {
				$thisurl = get_edit_term_link( $this_lang_term_id, $screen->taxonomy );
			} else {
				$thisurl = get_edit_term_link( $term_id, $screen->taxonomy );
				$thisurl = add_query_arg( 'lang_fail', $lang->slug, $thisurl );
			}
			
			if ( isset( $_GET['post_type'] ) ) {
				$thisurl = add_query_arg( 'post_type', $_GET['post_type'], $thisurl );
			}
		} else {
			$thisurl = esc_url( add_query_arg( 'lang', $lang->slug, $url ) );
		}
		
		$wp_admin_bar->add_menu(array(
			'parent' => 'languages',
			'id'     => $lang->slug,
			'title'  => $lang->flag . esc_html($lang->name),
			'href'   => $thisurl,
		));		
	}
}

function polylang_custom_admin_notice() {
	if ( ! is_object( $polylang ) ) { return; }
	global $polylang;
	
	$screen = get_current_screen();
	
	$selected = polylang_custom_get_selected_lang();
	
	if ( ! empty( $selected ) ) {
		if ( isset( $_GET['lang_fail'] ) ) {
			$new_lang = $polylang->model->get_language( $_GET['lang_fail'] );
		} else {
			$new_lang = $selected;
		}
		if ( $screen->base == 'post' ) {
			$post_id = get_the_ID();
			$post_type_object = get_post_type_object( $screen->post_type );
			if ( $screen->post_type == 'attachment' ) {
				$url = admin_url( 'admin.php?action=translate_media&from_media=' . $post_id . '&new_lang=' . $new_lang->slug );
			} else {
				$url = admin_url( 'post-new.php?post_type=' . $screen->post_type . '&from_post=' . $post_id . '&new_lang=' . $new_lang->slug );
			}
			$singular_name = $post_type_object->labels->singular_name;
		} else if ( $screen->base == 'edit-tags' && isset( $_GET['tag_ID'] ) ) {
			$taxonomy_object = get_taxonomies( '', 'objects' );
			$term_id = intval( $_GET['tag_ID'] );
			$url = admin_url( 'edit-tags.php?taxonomy=' . $screen->taxonomy . '&from_tag=' . $term_id . '&new_lang=' . $new_lang->slug );
			$singular_name = $taxonomy_object[$screen->taxonomy]->labels->singular_name;
		}
		if ( ! empty( $url ) ) {
			$url = add_query_arg( 'lang', $new_lang->slug, $url );
		}
    ?>
    <div class="updated">
        <p><?php
		printf( __( 'This %1$s is not available in %2$s %3$s. <a title="Add %3$s Translation" href="%4$s">Add %3$s Translation</a>', 'text-domain' ),
		$singular_name,
		$new_lang->flag,
		$new_lang->name ,
		$url 
		)
		?></p>
    </div>
    <?php
	}
}

function polylang_custom_init_admin(){
	global $polylang;
	if ( is_object( $polylang ) ) {
		if ( isset( $_GET['new_lang'] ) ) {
			if ( isset( $_GET['from_post'] ) && is_numeric( $_GET['from_post'] ) ) {
				$post_id = intval( $_GET['from_post'] );
				$post_type = get_post_type( $post_id );
				$new_lang_slug = $_GET['new_lang'];
				if ( pll_is_translated_post_type( $post_type ) ) {
					$selected = polylang_custom_get_selected_lang();
					if ( $selected->slug != $new_lang_slug ) {
						$url = admin_url( 'post-new.php?post_type='.$post_type.'&from_post=' . $post_id.'&new_lang='.$new_lang_slug );
						$url = add_query_arg( 'lang', $new_lang_slug, $url );
						wp_safe_redirect( $url );
						exit;
					}
				}
			} else if ( isset( $_GET['from_tag'] ) && is_numeric( $_GET['from_tag'] ) && isset( $_GET['post_type'] ) && isset( $_GET['taxonomy'] ) ) {
				$term_id = intval( $_GET['from_tag'] );
				$post_type = $_GET['post_type'];
				$taxonomy = $_GET['taxonomy'];
				$new_lang_slug = $_GET['new_lang'];
				$selected = polylang_custom_get_selected_lang();
				if ( $selected->slug != $new_lang_slug ) {
					$url = admin_url( 'edit-tags.php?taxonomy=' . $taxonomy . '&post_type=' . $post_type . '&from_tag=' . $term_id . '&new_lang=' . $new_lang_slug );
					$url = add_query_arg( 'lang', $new_lang_slug, $url );
					wp_safe_redirect( $url );
					exit;
				}
			}
		}
	}
}
add_action( 'admin_init', 'polylang_custom_init_admin', 1 );

function polylang_custom_get_selected_lang() {
	global $polylang;
	if ( is_object( $polylang ) ) {
	$all_item = (object) array(
		'slug' => 'all',
		'name' => __('Show all languages', 'polylang'),
		'flag' => sprintf('<img src="%s" style="margin-bottom: -3px"/>', POLYLANG_URL .'/flags/all.png')
	);

	$selected = !empty($_GET['lang']) && !is_numeric($_GET['lang']) && ($lang = $polylang->model->get_language($_GET['lang'])) ? $lang->slug :
		(($lg = get_user_meta(get_current_user_id(), 'pll_filter_content', true)) ? $lg : 'all');

	$selected = ('all' == $selected) ? $all_item : $polylang->model->get_language($selected);
	return $selected;
	}
	return false;	
}

function polylang_custom_edit_post_link( $url, $post_id, $context) {
	global $polylang;
	if ( is_object( $polylang ) ) {
		$post_language = $polylang->model->get_post_language( $post_id );
		if ( is_object( $post_language ) ) {
			if ( ! empty( $post_language->slug ) ) {
				$url = add_query_arg( 'lang', $post_language->slug, $url );
			}
		}
	}
    return $url;
}

function polylang_custom_edit_term_link( $url, $term_id, $tax) {
	global $polylang;
	if ( is_object( $polylang ) ) {
		$term_language = $polylang->model->get_term_language( $term_id );
		if ( is_object( $term_language ) ) {
			if ( ! empty( $term_language->slug ) ) {
				$url = add_query_arg( 'lang', $term_language->slug, $url );
			}
		}
	}
    return $url;
}
?>